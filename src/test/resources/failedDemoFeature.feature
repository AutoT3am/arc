@FeatureFailDemo
Feature: Test fail demo

 @FailedTest @High
 Scenario: Click on row
   Given I`m on a homepage
   When I enter in a text field called 'Enter search text' following text "unexisting company"
   And I click on table row with "unexisting company"
   Then I see that text field called 'Name' contains following text "unexisting company"
